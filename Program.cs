﻿using System;
using System.Net.Http.Headers;

namespace ConsoleApp1
{
    internal class Program
    {

        static void Main(string[] args)
        {
            while (true)
            {
                int select;
                Console.WriteLine(
                    "Select programm: \n" +
                    "[1] Division with reminder. \n" +
                    "[2] Bitwise multiplication.\n" +
                    "[3] Number Range. \n" +
                    "[4] Dictionary. \n" +
                    "[5] Calculator. \n" +
                    "[6] Matrix. \n" +
                    "[0] Exit programm. \n");
                select = InputCheckInt();
                switch (select)
                {
                    case 0:
                        Environment.Exit(0);
                        break;
                    case 1:
                        IsEven1();
                        break;
                    case 2:
                        IsEven2();
                        break;
                    case 3:
                        NumberRange();
                        break;
                    case 4:
                        //тут могла быть ваша реклама
                        Dictionary();
                        break;
                    case 5:
                        Calc();
                        break;
                    case 6:
                        Massive();
                        break;
                    default:
                        Console.WriteLine("There is no programm. Try one more time. \n");
                        break;
                }
            }
        }

        /// <summary>
        /// Check input value
        /// </summary>
        /// <returns>int</returns>
        static int InputCheckInt()
        {
            int num;
            Console.WriteLine("Input number: ");
            bool isCorrect = int.TryParse(Console.ReadLine(), out num);

            while (!isCorrect)
            {
                Console.WriteLine("Incorrect. Try one more time.\nInput number: ");
                isCorrect = int.TryParse(Console.ReadLine(), out num);
            }
            return num;
        }

        static double InputCheckDouble()
        {
            double num;
            Console.WriteLine("Input number: ");
            bool isCorrect = double.TryParse(Console.ReadLine(), out num);

            while (!isCorrect)
            {
                Console.WriteLine("Incorrect. Try one more time.\nInput number:");
                isCorrect = double.TryParse(Console.ReadLine(), out num);
            }
            return num;
        }

        /// <summary>
        /// Division with reminder
        /// </summary>
        static void IsEven1()
        {
            Console.WriteLine("\nDivision with reminder.\n");
            var num = InputCheckInt();

            if (num % 2 == 0)
            {
                Console.WriteLine($"\nThe number {num} is even\n");
            }
            else
            {
                Console.WriteLine($"\nThe number {num} is odd\n");
            }
        }
        //
        /// <summary>
        /// Bitwise multiplication with &
        /// </summary>
        static void IsEven2()
        {
            Console.WriteLine("\nBitwise multiplication.\n");
            var num = InputCheckInt();
            int inputNum = num;
            string result;

            num = num & 1;
            result = (num == 1) ? $"\nThe number {inputNum} is odd\n" : $"\nThe number {inputNum} is even\n"; //last time i use this shit...

            Console.WriteLine(result);
        }

        static void NumberRange()
        {
            Console.WriteLine("\nNumber Range.\n");
            Console.WriteLine("You need to inpun number in range 0 - 100\n");
            int num = InputCheckInt();

            if (num >= 0 && num <= 14)
            {
                Console.WriteLine("You are in range: [0-14]\n");
            }
            else if (num >= 15 && num <= 35)
            {
                Console.WriteLine("You are in range: [15-35]\n");
            }
            else if (num >= 36 && num <= 49)
            {
                Console.WriteLine("You are in range: [36-50]\n");
            }
            else if (num == 50)
            {
                Console.WriteLine("You are in two ranges: [36-50] [50-100]\n");
            }
            else if (num >= 51 && num <= 100)
            {
                Console.WriteLine("You are in range: [50-100]\n");
            }
            else
            {
                Console.WriteLine("You are out of range: [0-100]\n");
            }
        }

        //массивъ
        static void Massive()
        {
            Console.ForegroundColor = ConsoleColor.Green;

            int[,] array2D = new int[,]
            {
                { 4, 5, 3 },
                { 3, 6, 8 },
                { 1, 0, 2 }
            };

            int result = 0;

            for (int i = 0; i < array2D.GetLength(0); i++)
            {
                Console.WriteLine($"Index [{i}, {i}] of main diagonal: {array2D[i, i]}\n");
                result += array2D[i, i];
            }

            Console.WriteLine($"Addition result: {result}");
            Console.ForegroundColor = ConsoleColor.White;
        }

        /// <summary>
        /// Calculator
        /// </summary>
        static void Calc()
        {
            Console.WriteLine("\nCalculator.\n");
            double operand1;
            double operand2;
            string sign;
            double result = double.NaN; //if something doing wromg, we make result == NaN and write it in console

            operand1 = InputCheckDouble();
            Console.WriteLine("Inpit sign (*, /, +, -): \n");
            sign = Console.ReadLine();
            operand2 = InputCheckDouble();


            if (operand2 == 0 && sign == "/")
            {
                Console.Write("Division by zero is forbiden.\n");
            }
            else
            {
                switch (sign)
                {
                    case "+":
                        result = operand1 + operand2;
                        break;
                    case "-":
                        result = operand1 - operand2;
                        break;
                    case "*":
                        result = operand1 * operand2;
                        break;
                    case "/":
                        result = operand1 / operand2;
                        break;
                    default:
                        Console.WriteLine("Incorrect. Try one more time.\n");
                        break;
                }
            }
            if (result == double.NaN)
            {
                Console.WriteLine("something doing wromg\n");
            }
            else
            {
                Console.WriteLine($"Result: {result} \n");
            }
        }

        static void Dictionary()
        {
            Console.WriteLine("\nDictionary.\n");
            string[] dictionaryRUS = new string[]
            { "Облачно", "Солнечно", "Дождливо", "Ветренно", "Прохладно", "Холодно", "Жарко", "Морозно", "Туманно", "Сыро" };

            string[] dictionaryENG = new string[]
            { "Cloudy", "Sunny", "Rainy", "Windy", "Cool", "Cold", "Hot", "Frosty", "Foggy", "Wet" };

            var valueRUS = string.Join(", ", dictionaryRUS);

            Console.WriteLine($"I know 10 words: {valueRUS}\n Input word on RUS: \n");
            string searchString = Console.ReadLine();

            if (dictionaryRUS.Contains(searchString))
            {
                int indexRUS = Array.IndexOf(dictionaryRUS, searchString);
                Console.WriteLine($"\nTransale of this word is: {dictionaryENG.GetValue(indexRUS)}");
            }
            else
            {
                Console.WriteLine("I dont know this word. Try one more time.\n");
            }
        }
    }
}